---
home: true
profileImg: /jb-perfil.jpg
profileImgAlt: jb perfil foto
title: Homepage
footer: O Rodape??? E-mail?
---
Texto com descrição básico ao lado da foto. Deve ter até 300 toques e entre três a cinco linhas. O ideal é se possível sempre ser uma quebra de colunas e números ímpar.

Em dispositivos móveis o texto vai ter que ficar antes ou depois da foto. E as imagens de fundo ficam acima deles ou abaixo??
