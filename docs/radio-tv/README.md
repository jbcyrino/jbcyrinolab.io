---
title: Rádio e TV
sidebar: auto
---

# Rádio e TV

## Atletas de E-sports colocam Floripa em evidência e aquecem a economia local

Reportagem para TV apurada, narrada e editada por mim:

<iframe width="100%" height="315" src="https://www.youtube.com/embed/Ef9gTUEwbUY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<hr>

## DLC Insira a Ficha #29 - Cyrino, o profeta

Esse fim de semana contou com as pré-conferências da E3, e na terça a feira começa oficialmente. Nossos integrantes comentaram sobre suas expectativas
\~veremos se eles acertam algo né\~. Na apresentação, Luis Fernando Schmidt, com os comentários de Eduardo Melo, João Bosco Cyrino e Iraci Falavina. Na técnica, o sempre presente Peter Lobo.

<iframe width="100%" height="120" src="https://www.mixcloud.com/widget/iframe/?hide_cover=1&feed=%2Fradiopontoufsc%2Finsira-a-ficha-03062019%2F" frameborder="0" ></iframe>
